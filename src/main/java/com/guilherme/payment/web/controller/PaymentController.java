package com.guilherme.payment.web.controller;

import com.guilherme.payment.web.model.dto.PaymentDTO;
import com.guilherme.payment.web.model.entity.Payment;
import com.guilherme.payment.web.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import org.apache.log4j.Logger;

import java.util.*;

@RestController
@RequestMapping("/payment")
@EnableAutoConfiguration
public class PaymentController {

    private static final Logger logger = Logger.getLogger(PaymentController.class);

    @Autowired
    private PaymentRepository repository;

    @PostMapping("/solicitar-pagamento")
    String postPayment(@RequestBody PaymentDTO paymentDTO) {

        logger.debug(paymentDTO.toString());
        repository.save(new Payment(paymentDTO));
        return paymentDTO.toString();
    }

    @GetMapping("/consultar-pagamento/*")
    List<PaymentDTO> getPayment(@RequestParam(name="name", required=false) String name ) {
        logger.debug(name);
        List<PaymentDTO> listPayment = new ArrayList<PaymentDTO>();
        if(name != null && !name.isEmpty()) {
            repository.findByName(name).forEach(payment -> {
                listPayment.add(new PaymentDTO(payment));
            });
        }
        else {
            repository.findAll().forEach(payment -> {
                listPayment.add(new PaymentDTO(payment));
            });
        }
        return listPayment;
    }

    @RequestMapping(method = RequestMethod.PUT, value = {"/cancelar-pagamento", "/cancelar-pagamento/{idPaymentPath}"})
    String putPayment(@RequestHeader(value="idPayment", required = false) String idPaymentHeader, @RequestParam(name="idPayment", required=false) String idPaymentParam, @PathVariable Optional<String> idPaymentPath) {
        Payment payment = null;
        Optional<Payment> opt = null;
        if(idPaymentHeader != null) {
            logger.debug("idPaymentHeader: " + idPaymentHeader);
            opt = repository.findById(idPaymentHeader);
        }
        else if(idPaymentParam != null) {
            logger.debug("idPaymentParam: " + idPaymentParam);
            opt = repository.findById(idPaymentParam);
        }
        else if(idPaymentPath != null && idPaymentPath.isPresent()) {
            logger.debug("idPaymentPath: " + idPaymentPath.get());
            opt = repository.findById(idPaymentPath.get());
        }

        if(opt.isPresent()) {
            payment = opt.get();
            payment.setStatus("Canceled");
            repository.save(payment);
            return "Payment successfully canceled!";
        }
        else {
            return "Invalid id payment!";
        }

    }
}
