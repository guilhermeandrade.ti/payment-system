package com.guilherme.payment.web.repository;

import com.guilherme.payment.web.model.entity.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentRepository extends MongoRepository<Payment, String> {
	public List<Payment> findByName(String name);
}
