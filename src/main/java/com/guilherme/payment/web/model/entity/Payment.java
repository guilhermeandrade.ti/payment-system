package com.guilherme.payment.web.model.entity;

import com.guilherme.payment.web.model.dto.PaymentDTO;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Payment {
	@Id
	private String id;
	private String name;
	private String cardNumber;
	private String requestNumber;
	private String status;

	private Integer expirationMonth;
	private Integer expirationYear;
	private Integer cvv;

	private Double paymentValue;

	private Date paymentDate;

	public Payment() {
	}

	public Payment(PaymentDTO paymentDTO) {
		name = paymentDTO.getName();
		cardNumber = paymentDTO.getCardNumber();
		status = paymentDTO.getStatus();
		requestNumber = paymentDTO.getRequestNumber();
		expirationMonth = paymentDTO.getExpirationMonth();
		expirationYear = paymentDTO.getExpirationYear();
		cvv = paymentDTO.getCvv();
		paymentValue = paymentDTO.getPaymentValue();
		paymentDate = paymentDTO.getPaymentDate();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public Integer getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(Integer expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	public Integer getCvv() {
		return cvv;
	}

	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}

	public Double getPaymentValue() {
		return paymentValue;
	}

	public void setPaymentValue(Double paymentValue) {
		this.paymentValue = paymentValue;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
