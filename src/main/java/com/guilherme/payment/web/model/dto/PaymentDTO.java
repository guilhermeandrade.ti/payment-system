package com.guilherme.payment.web.model.dto;

import com.guilherme.payment.web.model.entity.Payment;

import java.util.Date;

public class PaymentDTO {
	private String id;
	private String name;
	private String cardNumber;
	private String requestNumber;
	private String status;

	private Integer expirationMonth;
	private Integer expirationYear;
	private Integer cvv;

	private Double paymentValue;

	private Date paymentDate;

	public PaymentDTO() {}

	public PaymentDTO(Payment payment) {
		id = payment.getId();
		name = payment.getName();
		cardNumber = payment.getCardNumber();
		requestNumber = payment.getRequestNumber();
		status = payment.getStatus();
		expirationMonth = payment.getExpirationMonth();
		expirationYear = payment.getExpirationYear();
		cvv = payment.getCvv();
		paymentValue = payment.getPaymentValue();
		paymentDate = payment.getPaymentDate();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(Integer expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}

	public Integer getCvv() {
		return cvv;
	}

	public void setCvv(Integer cvv) {
		this.cvv = cvv;
	}

	public Double getPaymentValue() {
		return paymentValue;
	}

	public void setPaymentValue(Double paymentValue) {
		this.paymentValue = paymentValue;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "PaymentDTO{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", cardNumber='" + cardNumber + '\'' +
				", paymentValue=" + paymentValue +
				", paymentDate=" + paymentDate +
				", status=" + status +
				'}';
	}
}
