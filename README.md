# Payment System

Bem vindo ao Payment System

Um sistema básico de pagamento, criado com Java, Spring Boot, Rest API, MLab. A aplicação está rodando no Heroku.

DOCKER implementado, necessário **TESTAR**!!

Para usá-la basta chamar os serviços abaixo:


**Health**

https://g-payment-system.herokuapp.com/actuator/health

Metodo: GET

Esse serviço serve para informar se a aplicação está funcionando

**Solicitar Pagamento**

https://g-payment-system.herokuapp.com/payment/solicitar-pagamento/

Metodo: POST

Headers:
*  Content-Type: application/json

Estrutura do BODY:
>  {
	"name" : "Guilherme Teste",
	"cardNumber" : "0000000000000000",
	"expirationMonth" : "08",
	"expirationYear" : "2020",
	"cvv" : "123",
	"paymentValue" : "500.30",
	"paymentDate" : "2019-03-31"
}

Esse serviço serve para solicitar pagamentos, ou seja, incluí-los no sistema, podendo receber um body, o serviço responderá com a mesma estrutura do body porém com o ID caso o
registro seja incluido com sucesso.

**Consultar Pagamento**

https://g-payment-system.herokuapp.com/payment/consultar-pagamento/

Metodo: GET

Parâmetros:
*  name - OPCIONAL - Texto

Esse serviço serve para obter informações de pagamentos, podendo receber como parâmetro "name", onde retornará somente pagamentos com o "name" informado,
ou caso não seja informado o "name" o serviço retornará todos os pagamentos do sistema.

**Cancelar Pagamento**

https://g-payment-system.herokuapp.com/payment/cancelar-pagamento/{ID_DO_PAGAMENTO}

Metodo: PUT

Header:
*  idPayment - OPCIONAL - Texto

Parâmetros:
*  idPayment - OPCIONAL - Texto

Esse serviço serve para mudar o status do Pagamento para cancelado, podendo receber o id do pagamento que se deseja cancelar através de 3 formas:
Via parâmetro, via header, ou via Path(Após a última barra "/" da URL).
o serviço responderá com a mesma estrutura do objeto Payment com o Status atualizado.
